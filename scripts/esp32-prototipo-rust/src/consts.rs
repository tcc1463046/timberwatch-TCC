pub const WIFI_SSID: &str = "Aluno Unisep";
pub const WIFI_PASSWORD: &str = "alunounisep";

// pub const WIFI_SSID: &str = "Ap 04";
// pub const WIFI_PASSWORD: &str = "6790**prolo";

// pub const WIFI_SSID: &str = "Segfy";
// pub const WIFI_PASSWORD: &str = "12fyseg3";

pub const TIMBERWATCH_SOCKET_URL: &str = "ws://192.168.0.102:4000/socket/websocket?vsn=2.0.0";
pub const TIMBERWATCH_MONITORING_TOPIC: &str = "room:monitoring";
pub const TIMBERWATCH_SENSOR_ID: &str = "esp32-1";
pub const TIMBERWATCH_SENSOR_NAME: &str = "ESP32 Protótipo";
pub const TIMBERWATCH_SENSOR_TYPE: &str = "temperature";
