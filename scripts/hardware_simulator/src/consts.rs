pub const SOCKET_URL: &str = "ws://localhost:4000/socket/websocket?vsn=2.0.0";
pub const MONITORING_CHANNEL: &str = "room:monitoring";

pub const SIMULATION_INTERVAL: u64 = 100;
