defmodule TimberWatchWeb.Layouts do
  use TimberWatchWeb, :html

  embed_templates "layouts/*"
end
