defmodule TimberWatchWeb.PageHTML do
  use TimberWatchWeb, :html

  embed_templates "page_html/*"
end
